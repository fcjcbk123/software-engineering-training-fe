import axios from 'axios';
import qs from 'query-string';

export interface Department {
  departmentNo: number;
  departmentName: string;
}

export function getDepartmentList() {
  return axios.post<Department[]>('/api/department/list');
}

export interface DepartmentInfo {
  deptNo: number;
  deptName: string;
  deptManagerId: number;
  higherDeptNo: number;
  employeeName: string;
  employeePhoneNumber: string;
  peopleNum: number;
}

export interface DepartmentParams extends Partial<DepartmentInfo> {
  pageNum: number;
  pageSize: number;
}

export interface DepartmentListRes {
  items: DepartmentInfo[];
  total: number;
}

export function queryDepartmentList(params: DepartmentParams) {
  return axios
    .get<DepartmentListRes>('/department/list', {
      params,
      paramsSerializer: (obj) => {
        return qs.stringify(obj);
      },
    })
    .then((response) => {
      // 去除幽灵部门
      response.data.items = response.data.items.filter(
        (item) => item.deptNo !== 44
      );
      return response;
    });
}

export interface DepartmentCreateForm {
  higherDeptNo?: number;
  deptName: string;
  deptManagerId: number;
  employeeName: string;
  employeePhoneNumber: string;
}

// Todo: api should be change
export function createDepartment(data: DepartmentCreateForm) {
  return axios.put('/department/dept_add', data);
}

export interface DepartmentUpdateData {
  deptNo: number;
  deptName: string;
  deptManagerId: number;
}

export interface DepartmentUpdateForm extends DepartmentUpdateData {
  employeeName: string;
  employeePhoneNumber: string;
}

// Todo: api should be change
export function updateDepartment(data: DepartmentUpdateForm) {
  return axios.post('/department/dept_change', data);
}

export function deleteDepartment(Id: number) {
  return axios.delete('/department/dept_delete', {
    params: {
      deptNo: Id,
    },
  });
}
