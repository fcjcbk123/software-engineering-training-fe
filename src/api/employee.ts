import axios from 'axios';
import qs from 'query-string';
import { UserState } from '@/store/modules/user/types';

export interface EmployeeRecord {
  employeeId: string;
  employeeName: string;
  employeePhoneNumber: string;
  employeeAvatar: string;
  employeeBirthday: Date;
  employeeGender: number;
  employeerole: string;
  employeeJob: string;
  deptNo: number;
  deptName: string;
}

export interface EmployeeFilter {
  name: string;
  role: string;
  deptNo: number;
  phoneNumber: string;
  gender: number;
  birthday: string;
}

export interface PolicyParams extends Partial<EmployeeFilter> {
  pageNum: number;
  pageSize: number;
}

export interface PolicyListRes {
  items: EmployeeRecord[];
  total: number;
}

export function uploadEmployees(File: File) {
  const formData = new FormData();
  formData.append('file', File);
  return axios.post('/employeeBasic/import_emp', formData, {
    headers: {
      'Content-Type': 'multipart/form-data',
    },
  });
}

export function queryPolicyList(params: PolicyParams, deptNo?: number) {
  return axios.get<PolicyListRes>('/employeeBasic/list', {
    params: {
      ...params,
      departmentNo: deptNo || params.deptNo,
    },
    paramsSerializer: (obj) => {
      return qs.stringify(obj);
    },
  });
}

export interface getEmployeeInfoReq {
  phone: string;
}

export function getEmployeeInfo(Id: string) {
  return axios.get<UserState>('/employeeBasic/info', {
    params: {
      employeeId: Id,
    },
  });
}

export function deleteEmployee(Id: string) {
  return axios.delete('/employeeBasic/delete_employee', {
    params: {
      employeeId: Id,
    },
  });
}

export function resetPassword(Id: string) {
  return axios.post('/employeeBasic/reset_password', {
    employeeId: Id,
  });
}

export function queryInspectionList() {
  return axios.get('/api/list/quality-inspection');
}

export function queryTheServiceList() {
  return axios.get('/api/list/the-service');
}

export function queryRulesPresetList() {
  return axios.get('/api/list/rules-preset');
}
