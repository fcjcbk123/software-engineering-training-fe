import axios from 'axios';

export interface BaseInfoModel {
  activityName: string;
  channelType: string;
  promotionTime: string[];
  promoteLink: string;
}
export interface ChannelInfoModel {
  advertisingSource: string;
  advertisingMedia: string;
  keyword: string[];
  pushNotify: boolean;
  advertisingContent: string;
}

export interface EmployeeBaseInfoModel {
  employeeName: string;
  employeeBirthday: string;
  employeePhoneNumber: string;
  employeeGender: number;
}

export interface EmployeeWorkInfoModel {
  employeeDepartmentNo: number;
  employeeJob: string;
  employeeRole: number;
}

export type UnitEmployeeModel = EmployeeBaseInfoModel & EmployeeWorkInfoModel;

// Todo: should be change
export function submitEmployeeForm(data: UnitEmployeeModel) {
  return axios.put('/employeeBasic/add', data);
}
