import html2canvas from 'html2canvas';
import JsPDF from 'jspdf';

const GeneratePDF = (dom: HTMLElement, fileName: string) => {
  html2canvas(dom, {
    allowTaint: true,
    useCORS: true, // 开启跨域
    width: dom.offsetWidth,
    height: dom.offsetHeight,
  }).then((canvas) => {
    const contentWidth = canvas.width;
    const contentHeight = canvas.height;

    // 设置PDF页面宽度和高度，单位为pt（1pt = 1/72in）
    const pageWidth = 595.28; // A4宽度
    const pageHeight = 841.89; // A4高度
    const imgWidth = pageWidth;
    const imgHeight = (pageWidth / contentWidth) * contentHeight;

    const pageData = canvas.toDataURL('image/jpeg', 1.0);
    const pdf = new JsPDF('portrait', 'pt', 'a4');

    let position = 0;
    if (imgHeight < pageHeight) {
      // 单页内容
      pdf.addImage(pageData, 'JPEG', 0, position, imgWidth, imgHeight);
    } else {
      // 多页内容
      while (position < contentHeight) {
        pdf.addImage(pageData, 'JPEG', 0, position, imgWidth, imgHeight);
        position -= pageHeight;
        if (position < contentHeight) {
          pdf.addPage();
        }
      }
    }
    pdf.save(`${fileName}.pdf`);
  });
};

export default GeneratePDF;
