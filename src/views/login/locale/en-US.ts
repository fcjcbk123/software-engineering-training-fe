export default {
  'login.form.title': 'Login to EmoTrack',
  'login.form.userName.errMsg': 'Username cannot be empty',
  'login.form.password.errMsg': 'Password cannot be empty',
  'login.form.login.errMsg': 'Login error, refresh and try again',
  'login.form.login.success': 'welcome to use',
  'login.form.userName.placeholder': 'Username: admin',
  'login.form.password.placeholder': 'Password: admin',
  'login.form.rememberPassword': 'Remember password',
  'login.form.forgetPassword': 'Forgot password',
  'login.form.login': 'login',
  'login.form.register': 'register account',
  'login.banner.slogan1': 'Manage Your Department',
  'login.banner.subSlogan1':
    'Low productivity? EmoTrack boosts your efficiency!',
  'login.banner.slogan2': 'Focus on Your Employees',
  'login.banner.subSlogan2':
    'How are they feeling? EmoTrack gives you quick insights!',
  'login.banner.slogan3': 'Improve Your Company',
  'login.banner.subSlogan3':
    "How's the atmosphere today? EmoTrack helps you enhance the environment!",
};
