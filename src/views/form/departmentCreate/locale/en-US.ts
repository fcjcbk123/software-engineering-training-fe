export default {
  'department.create.form.title': 'Create Department',
  'department.create.form.departmentName': 'Department Name',
  'department.create.form.departmentName.placeholder': 'Enter DeptName',
  'department.create.form.departmentName.required': 'DeptName is required',
  'department.create.form.managerPhone': 'Manager Phone',
  'department.create.form.managerPhone.placeholder': 'Enter phone number',
  'department.create.form.manager': 'Manager',
  'department.create.form.manager.placeholder': 'Enter manager name',
  'department.create.form.Id': 'Manager ID',
  'department.create.form.Id.placeholder': 'Enter manager ID',
};
