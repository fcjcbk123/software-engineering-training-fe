export default {
  'menu.detect': '情绪检测',
  'menu.detect.upload': '上传照片',
  'emotion.detect.upload': '上传照片',
  'emotion.detect.upload.title': '上传照片',
  'emotion.detect.upload.quit': '退出拍照',
  'emotion.detect.upload.capture': '拍照',
  'emotion.detect.upload.history': '查看历史记录',
  'emotion.detect.upload.format':
    '上传格式为jpg，png，bmp，pbm，pgm，ppm，sr，ras，ipeg，jpe，jp2，tiff文件',
  'emotion.detect.upload.information': '拖动照片到此处',
  'emotion.detect.upload.information.trigger': '点击此处上传',
  'emotion.detect.upload.again': '重新选择',
  'emotion.detect.upload.success.title': '上传照片成功',
  'emotion.detect.upload.success.subTitle': '情绪检测报告生成成功',
  'emotion.detect.upload.view': '查看报告',
  'emotion.detect.upload.button.again': '再次上传',
  'emotion.card.download.button': '下载报告',
};
