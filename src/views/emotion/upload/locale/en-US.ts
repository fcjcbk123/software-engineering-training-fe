export default {
  'menu.detect': 'Emotional Detection',
  'menu.detect.upload': 'Upload Photo',
  'emotion.detect.upload': 'Upload Photo',
  'emotion.detect.upload.title': 'Upload Photo',
  'emotion.detect.upload.quit': 'Exit Taking photos',
  'emotion.detect.upload.capture': 'Taking photos',
  'emotion.detect.upload.history': 'View History',
  'emotion.detect.upload.format':
    'Format requirement is jpg,png,bmp,pbm,pgm,ppm,sr,ras,ipeg,jpe,jp2,tiff',
  'emotion.detect.upload.information': 'Drag photos here',
  'emotion.detect.upload.information.trigger': 'Click here to upload',
  'emotion.detect.upload.again': 'Reselect',
  'emotion.detect.upload.success.title': 'Uploaded Successfully',
  'emotion.detect.upload.success.subTitle': 'Report generated successfully',
  'emotion.detect.upload.view': 'View Report',
  'emotion.detect.upload.button.again': 'Upload again',
  'emotion.card.download.button': 'Download',
};
