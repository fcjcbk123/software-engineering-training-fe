export default {
  'menu.visualization.multiDimensionDataAnalysis': 'Multi-D Analysis',
  'emotionAnalysis.card.title.dataOverview':
    "Department's detecting status today",
  'emotionAnalysis.dataOverview.contentProduction':
    'Number of detections today',
  'emotionAnalysis.dataOverview.contentClick': 'Volume of detections today',
  'emotionAnalysis.card.title.dataOverviewAll': 'Overall detection situation',
  'emotionAnalysis.dataOverview.contentProductionAll':
    'Total number of detections',
  'emotionAnalysis.dataOverview.contentClickAll': 'Total volume of detections',
  'emotionAnalysis.dataOverview.contentExposure':
    'Total number of normal emotions',
  'emotionAnalysis.dataOverview.activeUsers':
    'Number of abnormal emotions today',
  'emotionAnalysis.card.title.userActions': "Department's detecting status",
  'emotionAnalysis.card.title.contentTypeDistribution':
    'Emotional distribution',
  'emotionAnalysis.card.title.userRetention': 'Normal emotions',
  'emotionAnalysis.card.title.contentConsumption': 'Abnormal emotions',
  'emotionAnalysis.card.title.contentPublishingSource':
    'Emotional ratio in this department',
};
